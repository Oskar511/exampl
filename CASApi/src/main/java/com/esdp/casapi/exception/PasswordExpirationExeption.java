package com.esdp.casapi.exception;

public class PasswordExpirationExeption extends RuntimeException {
    public PasswordExpirationExeption(String message){
        super(message);
    }

}
