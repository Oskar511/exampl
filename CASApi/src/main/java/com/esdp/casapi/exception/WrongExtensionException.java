package com.esdp.casapi.exception;

public class WrongExtensionException extends RuntimeException {
    public WrongExtensionException(String message){
        super(message);
    }
}
