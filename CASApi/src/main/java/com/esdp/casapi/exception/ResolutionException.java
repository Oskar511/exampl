package com.esdp.casapi.exception;


public class ResolutionException extends RuntimeException{
    public ResolutionException(String message){
        super(message);
    }
}
