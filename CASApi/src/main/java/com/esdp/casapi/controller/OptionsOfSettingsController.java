package com.esdp.casapi.controller;

import com.esdp.casapi.model.OptionsOfSettings;
import com.esdp.casapi.repository.UserRepository;
import com.esdp.casapi.service.OptionsOfSettingsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.web.bind.annotation.*;

import org.springframework.transaction.annotation.Transactional;




@RestController
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, readOnly = false)

public class OptionsOfSettingsController {

    @Autowired
    private OptionsOfSettingsService optionsOfSettingsService;
    private OptionsOfSettings optionsOfSettings;
    @Autowired
    UserRepository userRepository;

    @PutMapping("/optionsOfSettings")
    @ApiOperation(value = "Добавление новох параметров настроек приложения")
    public ResponseEntity<OptionsOfSettings> saveOptions(@RequestBody OptionsOfSettings optionsOfSettings) {
        OptionsOfSettings optionsOfSettings1 = optionsOfSettingsService.saveOptions(optionsOfSettings);

        return new ResponseEntity<>(optionsOfSettings1, HttpStatus.CREATED);
    }
    @PostMapping("/giveAdminRoleToUser/{id}")
    public void giveAdminRoleToUser(@PathVariable("id") Long id){
        userRepository.giveAdminRoleToUser(id);
    }
}
