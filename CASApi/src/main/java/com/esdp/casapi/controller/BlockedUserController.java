package com.esdp.casapi.controller;

import com.esdp.casapi.model.BlockedUser;
import com.esdp.casapi.service.BlockedUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BlockedUserController {

    @Autowired
    private BlockedUserService blockedUserService;

    @PostMapping("/blockedUser")
    @ApiOperation(value = "Добавление новой записи блокировки в базу данных casapi")
    public ResponseEntity<BlockedUser> createBlockedUser(@Valid @RequestBody BlockedUser blockedUser, Authentication authentication) {
        BlockedUser blockedUser1 = blockedUserService.createBlockedUser(blockedUser, authentication);

        return new ResponseEntity<>(blockedUser1, HttpStatus.CREATED);
    }

    // =====================================Block University by university_id

    @PostMapping("/blockUniversityWithId/{id}")
    public ResponseEntity<String> blockUniversityById(@PathVariable("id") Long id, Authentication authentication) {
        String string = blockedUserService.blockUniversityWithId(id, authentication);

        return new ResponseEntity<>(string, HttpStatus.OK);
    }

    // =====================================Get blocked user by user_id

    @GetMapping("/getBlockedUserId/{id}")
    public ResponseEntity<BlockedUser> getBlockedUserId(@PathVariable("id") Long id) {
        BlockedUser blockedUser = blockedUserService.getBlockedUserWithUserId(id);

        return new ResponseEntity<>(blockedUser, HttpStatus.OK);
    }

    @GetMapping("/blockedUser/{id}")
    @ApiOperation(value = "Просмотр записи по блокировкам по id")
    public ResponseEntity<BlockedUser> readblockedUser(@PathVariable("id") Long id) {
        BlockedUser blockedUser = blockedUserService.readblockedUser(id);

        return new ResponseEntity<>(blockedUser, HttpStatus.OK);
    }

    @GetMapping("/blockedUser")
    @ApiOperation(value = "Просмотр записей всех блокировок")
    public ResponseEntity<List<BlockedUser>> getblockedUserList() {
        List<BlockedUser> blockedUserList = blockedUserService.getblockedUserList();

        return new ResponseEntity<>(blockedUserList, HttpStatus.ACCEPTED);
    }

    @PutMapping("/blockedUser/{id}")
    @ApiOperation(value = "Изменение записи блокировки по id")
    public ResponseEntity<BlockedUser> updatePerson(@RequestBody BlockedUser blockedUser, @PathVariable Long id) {
        BlockedUser blockedUser1 = blockedUserService.updatePerson(blockedUser, id);

        return new ResponseEntity<>(blockedUser1, HttpStatus.OK);
    }
}
