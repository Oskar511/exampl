package com.esdp.casapi.controller;

import com.esdp.casapi.model.User;
import com.esdp.casapi.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/user")
    @ApiOperation(value = "Добавление новой записи User")
    public ResponseEntity<User> saveUser(@RequestBody User user, Authentication authentication) {
        User user1 = userService.saveUser(user, authentication);

        return new ResponseEntity<>(user1, HttpStatus.CREATED);
    }

    @GetMapping("/user")
    @ApiOperation(value = "Просмотр всех пользователей")
    public ResponseEntity<List<User>> getUserList(Authentication authentication) {
        List<User> user1 = userService.getUserList(authentication);

        return new ResponseEntity<>(user1, HttpStatus.ACCEPTED);
    }

    @GetMapping("/getUsersOfUniversity/{id}")
    public ResponseEntity<List<User>> getUsersOfUniversity(@PathVariable("id") Long id) {
        List<User> user1 = userService.getUsersOfUniversity(id);

        return new ResponseEntity<>(user1, HttpStatus.ACCEPTED);

    }

    @GetMapping("/user/{id}")
    @ApiOperation(value = "Просмотр записи  User по id")
    public ResponseEntity<User> getUser(@PathVariable("id") long id, Authentication authentication) {
        User user1 = userService.getUser(id, authentication);
        return new ResponseEntity<>(user1, HttpStatus.OK);
    }

    @PutMapping("/user")
    @ApiOperation(value = "Изменение записи User по id")
    public ResponseEntity<User> updateUser(@RequestBody User user, Authentication authentication) {
        User user1 = userService.setUser(user, authentication);
        return new ResponseEntity<>(user1, HttpStatus.OK);
    }

    @DeleteMapping("/user/{id}")
    @ApiOperation(value = "Удаление записи User по id")
    public ResponseEntity<Void> updateUser(@PathVariable("id") long id) {
        {
            userService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.GONE);
        }

    }


}
