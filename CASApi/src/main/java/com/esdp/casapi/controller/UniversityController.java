package com.esdp.casapi.controller;

import com.esdp.casapi.model.University;
import com.esdp.casapi.service.UniversityService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class UniversityController {
    @Autowired
    private UniversityService universityService;

    @PostMapping("/university")
    @ApiOperation(value = "Добавление записи University")
    public ResponseEntity<University> createUniversity(@Valid @RequestBody University university) throws IOException {
        University university1 = universityService.createUniversity(university);

        return new ResponseEntity<>(university1, HttpStatus.CREATED);
    }

    @GetMapping("/university/{id}")
    @ApiOperation(value = "Просмотр записи University по id")
    public ResponseEntity<University> readUniversity(@PathVariable("id") Long id) {
        University university1 = universityService.readUniversity(id);
        return new ResponseEntity<>(university1, HttpStatus.OK);
    }

    @GetMapping("/university")
    @ApiOperation(value = "Просмотр всех записей University")
    public ResponseEntity<List<University>> getUniversityList() {
        List<University> universityList = universityService.getUniversityList();
        return new ResponseEntity<>(universityList, HttpStatus.ACCEPTED);
    }

    @PutMapping("/university")
    @ApiOperation(value = "Изменение записи University по id")
    public ResponseEntity<University> replaceUniversity(@RequestBody University university) {
        University university1 = universityService.replaceUniversity(university);

        return new ResponseEntity<>(university1, HttpStatus.OK);

    }
}
