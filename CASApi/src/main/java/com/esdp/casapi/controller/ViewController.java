package com.esdp.casapi.controller;

import com.esdp.casapi.model.View;
import com.esdp.casapi.service.ViewService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ViewController {
    @Autowired
    ViewService viewService;

    @PostMapping("/view")
    @ApiOperation(value = "Добавление новой записи View")
    public ResponseEntity<View> createView(@RequestBody View view) {
        View view1 = viewService.createView(view);

        return new ResponseEntity<>(view1, HttpStatus.CREATED);
    }

    @GetMapping("/view")
    @ApiOperation(value = "Просмотр всех записей View")
    public ResponseEntity<List<View>> getRoleList() {
        List<View> view1 = viewService.getViewList();

        return new ResponseEntity<>(view1, HttpStatus.ACCEPTED);
    }

    @GetMapping("/view/{id}")
    @ApiOperation(value = "Просмотр записи View по id")
    public ResponseEntity<View> getView(@PathVariable("id") long id) {
        View view1 = viewService.getView(id);

        return new ResponseEntity<>(view1, HttpStatus.OK);
    }

    @PutMapping("/view/{id}")
    @ApiOperation(value = "Изменение записи View по id")
    public ResponseEntity<View> updateView(@RequestBody View view, @PathVariable long id) {
        View view1 = viewService.updateView(view, id);

        return new ResponseEntity<>(view1, HttpStatus.OK);
    }
}
