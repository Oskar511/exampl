package com.esdp.casapi.controller;

import com.esdp.casapi.model.Person;
import com.esdp.casapi.repository.PersonRepository;
import com.esdp.casapi.repository.UserRepository;
import com.esdp.casapi.service.Convertor;
import com.esdp.casapi.service.PersonService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class PersonController {
    @Autowired
    private PersonService personService;
    @Autowired
    UserRepository userRepo;
    @Autowired
    PersonRepository personRepo;
    @Autowired
    Convertor convertor;

    @PostMapping("/person")
    @ApiOperation(value = "Добавление новой записи Person")
    public ResponseEntity<Person> createPerson(@Valid @RequestBody Person person, Authentication authentication) throws IOException {
        Person person1 = personService.createPerson(person, authentication);

        return new ResponseEntity<>(person1, HttpStatus.CREATED);
    }

    @PostMapping(value = "/uploadPhoto", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation(value = "Загрузка фото")
    public ResponseEntity<String> uploadPhoto(@RequestBody MultipartFile photo, Authentication authentication) throws IOException {
        String photo1 = personService.uploadPhoto(photo, authentication);

        return new ResponseEntity<>(photo1, HttpStatus.OK);
    }

    @GetMapping("/person/{id}")
    @ApiOperation(value = "Просмотр записи  Person по id")
    public ResponseEntity<Person> readPerson(@PathVariable("id") Long id, Authentication authentication) {
        Person person1 = personService.readPerson(id, authentication);

        return new ResponseEntity<>(person1, HttpStatus.OK);
    }

    @GetMapping("/person")
    @ApiOperation(value = "Просмотр всех записей Person")
    public ResponseEntity<List<Person>> getPersonList(Authentication authentication) {
        List<Person> list1 = personService.getPersonList(authentication);

        return new ResponseEntity<>(list1, HttpStatus.ACCEPTED);
    }

    @PutMapping("/person")
    @ApiOperation(value = "Изменение записи Person по id")
    public ResponseEntity<Person> updatePerson(@RequestBody Person person, Authentication authentication) {
        Person person1 = personService.updatePerson(person, authentication);

        return new ResponseEntity<>(person1, HttpStatus.OK);
    }
}
