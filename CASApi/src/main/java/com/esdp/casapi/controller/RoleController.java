package com.esdp.casapi.controller;

import com.esdp.casapi.model.Role;
import com.esdp.casapi.service.RoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class RoleController {
    @Autowired
    private RoleService roleService;

    @PostMapping("/role")
    @ApiOperation(value = "Добавление новой записи Role")
    public ResponseEntity<Role> createRole(@RequestBody Role role) {
        Role role1 = roleService.createRole(role);

        return new ResponseEntity<>(role1, HttpStatus.CREATED);
    }

    @GetMapping("/role")
    @ApiOperation(value = "Просмотр всех записей Role")
    public ResponseEntity<List<Role>> getRoleList() {
        List<Role> role1 = roleService.getRoleList();

        return new ResponseEntity<>(role1, HttpStatus.ACCEPTED);
    }

    @GetMapping("/role/{id}")
    @ApiOperation(value = "Просмотр записи Role по id")
    public ResponseEntity<Role> getRole(@PathVariable("id") long id) {
        Role role1 = roleService.getRole(id);

        return new ResponseEntity<>(role1, HttpStatus.OK);
    }

    @PutMapping("/role/{id}")
    @ApiOperation(value = "Изменение записи Role по id")
    public ResponseEntity<Role> updateRole(@RequestBody Role role, @PathVariable long id) {
        Role role1 = roleService.updateRole(role, id);

        return new ResponseEntity<>(role1, HttpStatus.OK);
    }
}
