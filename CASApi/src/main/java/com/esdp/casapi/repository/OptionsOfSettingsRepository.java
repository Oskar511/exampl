package com.esdp.casapi.repository;

import com.esdp.casapi.model.BlockedUser;
import com.esdp.casapi.model.OptionsOfSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OptionsOfSettingsRepository extends JpaRepository<OptionsOfSettings, Long> {
    @Query(value=" SELECT options_of_settings.max_amount_of_users from options_of_settings ",nativeQuery = true)
    Integer maxAmountOfUsers ();
}
