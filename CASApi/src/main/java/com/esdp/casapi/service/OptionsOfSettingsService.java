package com.esdp.casapi.service;

import com.esdp.casapi.model.OptionsOfSettings;
import com.esdp.casapi.repository.OptionsOfSettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OptionsOfSettingsService {

    @Autowired
    private OptionsOfSettingsRepository optionsOfSettingsRepository;

    public OptionsOfSettings saveOptions(OptionsOfSettings optionsOfSettings) {
        return optionsOfSettingsRepository.save(optionsOfSettings);
    }

    public Integer maxAmountOfUsers () {
        return optionsOfSettingsRepository.maxAmountOfUsers();
    }
}
