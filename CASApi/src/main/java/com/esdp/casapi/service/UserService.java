package com.esdp.casapi.service;

import com.esdp.casapi.config.StatusEnum.Status;
import com.esdp.casapi.exception.CountOfStudentsIsOver;
import com.esdp.casapi.exception.PasswordValadateExeption;
import com.esdp.casapi.exception.WronUniversityNameException;
import com.esdp.casapi.model.Person;
import com.esdp.casapi.model.Role;
import com.esdp.casapi.model.University;
import com.esdp.casapi.model.User;
import com.esdp.casapi.repository.PersonRepository;
import com.esdp.casapi.repository.RoleRepository;
import com.esdp.casapi.repository.UniversityRepository;
import com.esdp.casapi.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, readOnly = false)
public class UserService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    UniversityRepository universityRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    GetCurrentUser getCurrentUser;

    private Logger logger = LoggerFactory.getLogger(this.getClass());// для zipkin

    public User saveUser(User user, Authentication authentication) {

//        logger.info("user -> {}",userRepo.save(user));

        String userPassword = user.getPassword();
        if(!userPassword.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}")) {
            throw new PasswordValadateExeption("Пароль слишком легкий");
        }

        User admin = getCurrentUser.getCurrentUser(authentication);
        user.setPassword(encoder.encode(user.getPassword()));
        user.setStatus(Status.ACTIVE);

        if(admin.getUniversity()!=null) {
            user.setUniversity(admin.getUniversity());
        }
        if (user.getRoles() == null) {
            Set<Role> defaultRoleStudent = new HashSet(Arrays.asList(roleRepository.findByName("STUDENT")));
            user.setRoles(defaultRoleStudent);
        }
        if (admin.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN"))) {
            user.setPassword(encoder.encode(user.getPassword()));
            userRepo.updatePassword_expiration(user.getId());
            User savedUser = userRepo.save(user);
            University university = universityRepository.findByName(user.getUniversity());
            if(university==null){
                personRepository.save(new Person(savedUser.getId(), 0L, savedUser.getId()));
            }else {
                personRepository.save(new Person(savedUser.getId(), university.getID(), savedUser.getId()));
            }
            return savedUser;
        }
        if (userRepo.countStudentsOfUniversity(user.getUniversity()) >= 100) {
            throw new CountOfStudentsIsOver("Количество студентов указанного университета превышает установленный лимит в системе!");
        }
        if (admin.getUniversity().equals(user.getUniversity())) {
            user.setPassword(encoder.encode(user.getPassword()));
            userRepo.updatePassword_expiration(user.getId());
            User savedUser = userRepo.save(user);
            University university = universityRepository.findByName(user.getUniversity());
            personRepository.save(new Person(savedUser.getId(), university.getID(),savedUser.getId()));
            return savedUser;

        } else {
            throw new WronUniversityNameException("Неправильное название университета!");
        }
    }


    public List<User> getUserList(Authentication authentication) {
        logger.info("user -> {}",userRepo.findAll());
        User user = getCurrentUser.getCurrentUser(authentication);

        if(user.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN"))){
            return userRepo.findAll();
        }
            return userRepo.findByUniversity(user.getUniversity());

    }


    public List<User> getUsersOfUniversity(Long id)
    {
        return userRepo.getUsersWithUniversityId(id);
    }


    public User getUser(long id, Authentication authentication) {

        User user = getCurrentUser.getCurrentUser(authentication);

        if(user.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN"))){
            return userRepo.findById(id).get();
        }
        if(user.getRoles().stream().anyMatch(role -> role.getName().equals("ADMIN"))) {
            return userRepo.findByIdAndUniversity(id, user.getUniversity());
        }
        else{
            return userRepo.findById(user.getId()).get();
        }
    }

    public User setUser(User user, Authentication authentication){
        String userPassword = user.getPassword();
        if(!userPassword.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}")) {
            throw new PasswordValadateExeption("Пароль слишком легкий");
        }

        User admin = getCurrentUser.getCurrentUser(authentication);

        String adminUniversity = admin.getUniversity();
        String userUniversity = user.getUniversity();

        if (admin.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN")) || adminUniversity.equals(userUniversity)) {
            User putUser = userRepo.findById(user.getId())
                    .map(user_ -> {
                        user_.setUsername(user.getUsername());
                        user_.setPassword(encoder.encode(user.getPassword()));
                        return userRepo.save(user_);
                    }).get();
//                logger.info("user -> {}",putUser);
            userRepo.updatePassword_expiration(putUser.getId());//обновление даты для отображения, нужно чо то тут сделать чтобы на экране и в логике дке разных не было
            return putUser;
        } else {
            throw new WronUniversityNameException("Неправильное название университета!");
        }
    }

    public void deleteById(long id){
        personRepository.changeStatusToDeleted(id);
         userRepo.changeStatusToDeleted(id);
    }

    public boolean isPasswordExpiration(long id){
        LocalDate lastPasswordChangeDate = userRepo.getPasswordExpiration(id);
        LocalDate dateNow = LocalDate.now();

        if((dateNow.minusDays(30)).isBefore(lastPasswordChangeDate)){
            return true;
        }else {
            return false;
        }
    }
}