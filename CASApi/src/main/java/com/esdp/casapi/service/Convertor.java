package com.esdp.casapi.service;

import com.esdp.casapi.exception.NoPhotoException;
import com.esdp.casapi.exception.ResolutionException;
import com.esdp.casapi.exception.SizeException;
import com.esdp.casapi.exception.WrongExtensionException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;

@Service
public class Convertor {

	private Logger logger = LoggerFactory.getLogger(this.getClass());// для zipkin


	public String convertToBase64 (MultipartFile file) throws IOException {

		byte[] fileContent = photoValidate(file).getBytes();
		String encodedString = Base64.getEncoder().encodeToString(fileContent);
		logger.info("encodedString -> {}",encodedString);
		return encodedString;

	}


	private MultipartFile photoValidate(MultipartFile photo) throws IOException {

		if(!photo.getContentType().equals("image/jpeg")){
			if(!photo.getContentType().equals("image/jpg")){
				if(!photo.getContentType().equals("image/png")){
					throw new WrongExtensionException("Неправильный формат файла");
				}
			}
		}

		if (photo.getSize() > 1048576)
			throw new SizeException("слишком большой размер файла");
		BufferedImage img = ImageIO.read(photo.getInputStream());

		int width          = img.getWidth();
		int height         = img.getHeight();
		System.out.println(width);
		System.out.println(height);
		if (width > 354 && height > 472)
			throw new ResolutionException("фото должно быть 354х472");
		logger.info("photo -> {}",photo);

		return photo;
	}

//вытаскивает из базы файл и сохраняет его в папку
	public File convertToFile (String encodedString, String namePhoto,  String directory) {
		String outputFileName = "src/main/resources/"+directory+"/"+namePhoto;
		byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
		File file = new File(outputFileName);
		try (FileOutputStream fos = new FileOutputStream(file)) {
			fos.write(decodedBytes);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		logger.info("decode -> {}",file);

		return file;
	}

}