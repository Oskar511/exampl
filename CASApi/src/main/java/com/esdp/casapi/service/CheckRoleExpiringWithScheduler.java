package com.esdp.casapi.service;

import com.esdp.casapi.repository.OptionsOfSettingsRepository;
import com.esdp.casapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, readOnly = false)
public class CheckRoleExpiringWithScheduler {

    @Autowired
    OptionsOfSettingsRepository optionsOfSettingsRepository;
    @Autowired
    UserRepository userRepository;


    @Scheduled(cron = "0 0 3 ? * *")
    public void  checkExpiredRoles(){
        LocalDate roleExpiringDate = optionsOfSettingsRepository.findById(1L).get().getRoleDurationTime();
        if(roleExpiringDate.isBefore(LocalDate.now())){
            userRepository.expireAdminRoles();
        }
    }
}
