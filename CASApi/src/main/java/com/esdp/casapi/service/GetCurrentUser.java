package com.esdp.casapi.service;

import com.esdp.casapi.model.User;
import com.esdp.casapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class GetCurrentUser {
    @Autowired
    UserRepository userRepo;

    public User getCurrentUser(Authentication authentication){

        org.springframework.security.core.userdetails.User currentUser = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
        User user = userRepo.findByUsername(currentUser.getUsername());

        return user;
    }
}
