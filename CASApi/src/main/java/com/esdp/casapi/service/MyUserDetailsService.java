package com.esdp.casapi.service;

import com.esdp.casapi.exception.PasswordExpirationExeption;
import com.esdp.casapi.model.User;
import com.esdp.casapi.repository.BlockedUserRepository;
import com.esdp.casapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Component
public class MyUserDetailsService  implements UserDetailsService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private BlockedUserRepository blockedUserRepo;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepo.findByUsername(username);

        if(userService.isPasswordExpiration(user.getId())) {
            List<String> roles = new ArrayList<>();

            user.getRoles().stream().forEach(r -> roles.add(r.getName()));

                return org.springframework.security.core.userdetails.User
                        .withUsername(user.getUsername())
                        .password(user.getPassword())
                        .accountLocked(blockedUserRepo.findByUserId(user.getId())!=null||user.getStatus().equals("DELETED"))
                        .roles(roles.toArray(new String[roles.size()]))
                        .build();
        }else {
            throw new PasswordExpirationExeption("пароль устарел");
        }
    }
}
