package com.esdp.casapi.service;

import com.esdp.casapi.model.BlockedUser;
import com.esdp.casapi.model.User;
import com.esdp.casapi.repository.BlockedUserRepository;
import com.esdp.casapi.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class BlockedUserService {

    @Autowired
    private BlockedUserRepository blockedUserRepository;

    @Autowired
    GetCurrentUser getCurrentUser;

    @Autowired
    private UserRepository userRepository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());// для zipkin

    public BlockedUser createBlockedUser(@Valid @RequestBody BlockedUser blockedUser, Authentication authentication)
    {
        User admin = getCurrentUser.getCurrentUser(authentication);

        blockedUser.setManagerId(admin.getId());

        logger.info("blockedUser -> {}",  blockedUserRepository.save(blockedUser));
        return blockedUserRepository.save(blockedUser);
    }
    //==========================================Scheduling | unblocking users if time expired
    @Scheduled(cron = "0 * * ? * *")
    public void unBlockUsersIfTimesOut()
    {
        List<BlockedUser> blockedUsers = blockedUserRepository.findAll();
        List<BlockedUser> expiredUsers = new ArrayList<>();

        if(blockedUsers != null)
        {
            for(BlockedUser u : blockedUsers)
            {
                if(u.getBlockDate() != null && u.getBlockDate().isBefore(LocalDate.now()))
                {
                    expiredUsers.add(u);
                }
            }
        }
        if(expiredUsers != null)
        {
            blockedUserRepository.deleteInBatch(expiredUsers);
        }

    }
    //==========================================Get Blocked User with specified User Id

    public BlockedUser getBlockedUserWithUserId(Long id)
    {
       return blockedUserRepository.findByUserId(id);
    }
    //==========================================Block University with specified id

    public String blockUniversityWithId(Long id, Authentication authentication)
    {

        User admin = getCurrentUser.getCurrentUser(authentication);

        List<User> users = userRepository.getUsersWithUniversityId(id);
        List<BlockedUser> blockedUsers =  new ArrayList<BlockedUser>();

        for(User user : users)
        {
            BlockedUser bUser = new BlockedUser(user.getId(), LocalDate.now().plusYears(10), "blocked university");
            bUser.setManagerId(admin.getId());
            blockedUsers.add(bUser);
        }
        blockedUserRepository.saveAll(blockedUsers);
        return "Done";
    }
    //=================================================
    public BlockedUser readblockedUser(@PathVariable("id") Long id) {
        logger.info("blockedUser -> {}",  blockedUserRepository.findById(id).orElse(new BlockedUser()));
        return blockedUserRepository.findById(id).orElse(new BlockedUser());
    }

    public List<BlockedUser> getblockedUserList() {
        logger.info("blockedUser -> {}", blockedUserRepository.findAll());

        return blockedUserRepository.findAll();
    }


    public BlockedUser updatePerson(@RequestBody BlockedUser blockedUser, @PathVariable Long id) {
        BlockedUser putBlockedUser = blockedUserRepository.findById(id)
                .map(element -> {
                    element.setId(id);
                    element.setUserId(blockedUser.getUserId());
                    element.setManagerId(blockedUser.getManagerId());
                    element.setBlockDate(blockedUser.getBlockDate());
                    element.setDescription(blockedUser.getDescription());
                    return blockedUserRepository.save(blockedUser);
                })
                .orElseGet(() -> {
                    blockedUser.setId(id);
                    return blockedUserRepository.save(blockedUser);
                });
        logger.info("blockedUser -> {}", putBlockedUser);
        return putBlockedUser;
    }
}
