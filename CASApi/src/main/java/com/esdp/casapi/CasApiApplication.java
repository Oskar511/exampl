package com.esdp.casapi;

import brave.sampler.Sampler;
import com.esdp.casapi.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableConfigurationProperties(FileStorageProperties.class)
@EnableScheduling
@EnableSwagger2
public class CasApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CasApiApplication.class, args);
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public Sampler defaultSampler(){
        return Sampler.ALWAYS_SAMPLE;
    }

    @Bean
    public Docket productApi() {

        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        ApiInfo metaDate = new ApiInfo(

                "CasApi Documentation",
                "CasApi Documentation",
                "1.0",
                "terms and conditions",
                new Contact("JavaGroup1","",""),
                "",
                ""
        );

        return docket.select().apis(RequestHandlerSelectors
                .basePackage("com.esdp.casapi.controller"))
                .build().apiInfo(metaDate);
    }

}
