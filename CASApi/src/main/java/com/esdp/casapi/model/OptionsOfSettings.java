package com.esdp.casapi.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import java.time.LocalDate;


    @Entity
    @Table(name = "Options_Of_Settings")
    @ApiModel(value = "Параметры настроек приложения")
    public class OptionsOfSettings {
        @Id
        @SequenceGenerator(name = "my_seq_options_Of_Settings", sequenceName = "my_seq_options_Of_Settings", initialValue = 1, allocationSize = 1)
        @GeneratedValue(generator = "my_seq_options_Of_Settings")
        @Column(name = "settings_options_id")
        private long id;

        @Column(name = "max_amount_of_users")
        private Integer maxAmountOfUsers;

        @Column(name = "session_timeout_minute")
        private Integer sessionTimeoutMinute;

        @Column(name = "password_time")
        private Integer passwordTime;

        @Column(name = "role_duration_time")
        private LocalDate roleDurationTime;

        public OptionsOfSettings() {
        }

        public OptionsOfSettings(Integer maxAmountOfUsers, Integer sessionTimeoutMinute, Integer passwordTime, LocalDate roleDurationTime) {
            this.maxAmountOfUsers = maxAmountOfUsers;
            this.sessionTimeoutMinute = sessionTimeoutMinute;
            this.passwordTime = passwordTime;
            this.roleDurationTime = roleDurationTime;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public Integer getMaxAmountOfUsers() {
            return maxAmountOfUsers;
        }

        public void setMaxAmountOfUsers(Integer maxAmountOfUsers) {
            this.maxAmountOfUsers = maxAmountOfUsers;
        }

        public Integer getSessionTimeoutMinute() {
            return sessionTimeoutMinute;
        }

        public void setSessionTimeoutMinute(Integer sessionTimeoutMinute) {
            this.sessionTimeoutMinute = sessionTimeoutMinute;
        }

        public Integer getPasswordTime() {
            return passwordTime;
        }

        public void setPasswordTime(Integer passwordTime) {
            this.passwordTime = passwordTime;
        }

        public LocalDate getRoleDurationTime() {
            return roleDurationTime;
        }

        public void setRoleDurationTime(LocalDate roleDurationTime) {
            this.roleDurationTime = roleDurationTime;
        }

        @Override
        public String toString() {
            return "OptionsOfSettings{" +
                    "id=" + id +
                    ", maxAmountOfUsers=" + maxAmountOfUsers +
                    ", sessionTimeoutMinute=" + sessionTimeoutMinute +
                    ", passwordTime=" + passwordTime +
                    ", roleDurationTime=" + roleDurationTime +
                    '}';
        }
    }
