package com.esdp.casapi.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Set;


@Entity
@Table(name = "person")
@ApiModel(value = "Модель персональных данных")
public class Person {
    @Id
//    @SequenceGenerator(name = "mySeqGenPerson", sequenceName = "mySeqPerson", initialValue = 1, allocationSize = 1)
//    @GeneratedValue(generator = "mySeqGenPerson")
    @Column(name = "person_id")
    private long id;

    @Column
    private String status;

    @Column
    private String personalNumber;

    @Column
    @Size(min = 2, max = 30)
//    @Pattern(regexp = "[A-Za-zА-Яа-яёЁ\\s-]*")
//    @NotEmpty(message = "Please provide a name")
//    @NotNull(message = "Field can not be null")
    private String name;

    @Column
    @Size(min = 2, max = 30)
//    @Pattern(regexp = "[A-Za-zА-Яа-яёЁ\\s-]*")
//    @NotEmpty(message = "Please provide a surname")
//    @NotNull(message = "Field can not be null")
    private String surname;

    @Column
    private String middleName;

    @Column(columnDefinition = "text")
    private String photo;

    @Column
    private String faculty;

    @Column
    private String department;

    @Column
    private String type;

    @Column
    private LocalDate registeredDate;

    @Column
    private Long universityId;

    @Column
    private Long userId;


    public Person() {
    }
    public Person(Long id, Long universityId, Long userId){
        this.id = id;
        this.universityId = universityId;
        this.userId = userId;
    }

    public Person(String status, String personalNumber, @Size(min = 2, max = 30) @Pattern(regexp = "[A-Za-zА-Яа-яёЁ\\s-]*") @NotEmpty(message = "Please provide a name") @NotNull(message = "Field can not be null") String name, @Size(min = 2, max = 30) @Pattern(regexp = "[A-Za-zА-Яа-яёЁ\\s-]*") @NotEmpty(message = "Please provide a surname") @NotNull(message = "Field can not be null") String surname, String middleName, String photo, String faculty, String department, String type, LocalDate registeredDate, Long universityId, Long userId) {
        this.status = status;
        this.personalNumber = personalNumber;
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.photo = photo;
        this.faculty = faculty;
        this.department = department;
        this.type = type;
        this.registeredDate = registeredDate;
        this.universityId = universityId;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDate getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(LocalDate registeredDate) {
        this.registeredDate = registeredDate;
    }

    public Long getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Long universityId) {
        this.universityId = universityId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", status='" + status + '\'' +
                ", personalNumber='" + personalNumber + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", photo='" + photo + '\'' +
                ", faculty='" + faculty + '\'' +
                ", department='" + department + '\'' +
                ", type='" + type + '\'' +
                ", registeredDate=" + registeredDate +
                ", universityId=" + universityId +
                ", userId=" + userId +
                '}';
    }
}
