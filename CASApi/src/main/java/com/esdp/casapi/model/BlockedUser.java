package com.esdp.casapi.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "BlockedUsers")
@ApiModel(value = "Модель блокировки пользователей")
public class BlockedUser {

    @Id
    @SequenceGenerator(name = "mySeqGenBlocked", sequenceName = "mySeqBlocked", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "mySeqGenBlocked")
    private Long id;
    @Column
    private Long userId;
    @Column
    private Long managerId;
    @Column
    private LocalDate unblockDate;
    @Column(columnDefinition = "text")
    @NotEmpty(message = "Please provide a surname")
    @NotNull(message = "Field can not be null")
    private String description;

    public BlockedUser() {
    }

    public BlockedUser(Long userId, LocalDate unblockDate, @NotEmpty(message = "Please provide a surname") @NotNull(message = "Field can not be null") String description) {
        this.userId = userId;
        this.unblockDate = unblockDate;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public LocalDate getBlockDate() {
        return unblockDate;
    }

    public void setBlockDate(LocalDate blockDate) {
        this.unblockDate = blockDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "BlockedUser{" +
                "id=" + id +
                ", userId=" + userId +
                ", managerId=" + managerId +
                ", unblockDate=" + unblockDate +
                ", description='" + description + '\'' +
                '}';
    }
}
