package com.esdp.casapi.model;

import io.swagger.annotations.ApiModel;
import org.apache.catalina.User;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "universities")
@ApiModel(value = "Модель ВУЗов")
public class University {

    @Id
	@SequenceGenerator(name = "mySeqGenUniversity", sequenceName = "mySeqUniversity", initialValue = 1, allocationSize = 1)
	@GeneratedValue(generator = "mySeqGenUniversity")
    @Column(name = "university_id")
    private long ID;

    @Column(name = "status")
    private String status;

    @Column(name = "universityCode")
    private String universityCode;

    @Column(name = "name")
    @Size(min = 2, max = 30)
    @Pattern(regexp = "[A-Za-zА-Яа-яёЁ]*")
    @NotEmpty(message = "Please provide a name")
    @NotNull(message = "Field can not be null")
    private String name;

    @Column(name = "address")
    @NotEmpty(message = "Please provide a address")
    @NotNull(message= "example: Bishkek, Abdrahmanova str., 100")
    private String address;

    @Column(name = "registeredDate")
    @Past(message = "Registration date should not be a future time!")
    private LocalDate registeredDate;

    @Column(name = "logotype",columnDefinition = "text")
    private String logotype;

    public University() {
    }

    public University(String status, String universityCode, String name, String address, LocalDate registeredDate, Set<User> users, String logotype) {
        this.status = status;
        this.universityCode = universityCode;
        this.name = name;
        this.address = address;
        this.registeredDate = registeredDate;
        this.logotype = logotype;
    }
    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUniversityCode() {
        return universityCode;
    }

    public void setUniversityCode(String universityCode) {
        this.universityCode = universityCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(LocalDate registeredDate) {
        this.registeredDate = registeredDate;
    }

    public String getLogotype()
    {
        return logotype;
    }

    public void setLogotype(String logotype)
    {
        this.logotype = logotype;
    }

    @Override
    public String toString() {
        return "University{" +
                "ID=" + ID +
                ", status='" + status + '\'' +
                ", universityCode='" + universityCode + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", registeredDate=" + registeredDate +
                '}';
    }
}
