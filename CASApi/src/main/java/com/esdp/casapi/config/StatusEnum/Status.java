package com.esdp.casapi.config.StatusEnum;

public enum Status {
    DELETED, ACTIVE, RIGHT_EXPIRED;
}
