create table Options_Of_Settings (settings_options_id int8 not null, max_amount_of_users int4, session_timeout_minute int4, password_time int4, role_duration_time date);
create sequence my_seq_options_Of_Settings start 1 increment 1;
insert into Options_Of_Settings(settings_options_id, max_amount_of_users, session_timeout_minute, password_time, role_duration_time) values(1,100,100,100,'2019-01-01');
insert into roles values (5,'Админ у которого закончились  права', 'ADMIN_EXPIRED');